import logo from "./logo.svg";
import "./App.css";
import { useState, useEffect } from "react";
import axios from "axios";
import NewCustomer from "./components/NewCustomer";

function App() {
  const [customers, setCustomers] = useState([]);
  const [activeCustomer, setActiveCustomer] = useState(null);

  const loadCustomers = async () => {
    console.log("loadCustomers() called...");

    const result = await axios.get("http://localhost:4000/customers");

    if (!result.data) {
      setCustomers([]);
      return;
    }

    setCustomers(result.data.items);
  };

  // Lifecycle Hook
  useEffect(() => {
    loadCustomers();
  }, []);

  const handleDelete = async (customer) => {
    console.log("handleDelete() called...");

    await axios.delete(`http://localhost:4000/customers/${customer._id}`);

    loadCustomers();
  };

  const handleEdit = async (customer) => {
    console.log("handleEdit() called...");

    setActiveCustomer(customer);
  };

  // JSX -> React Template Language
  return (
    <div className="App container">
      <header className="App-header">
        <h3 className="my-3">Kundenverwaltung</h3>

        {customers &&
          customers.map((customer) => {
            return (
              <div
                key={customer._id}
                className="my-2 text-bg-light rounded-2 p-3"
              >
                <strong>
                  {customer.firstName} {customer.lastName}
                </strong>
                <button
                  className="btn btn-success btn-sm ms-2"
                  onClick={() => {
                    handleEdit(customer);
                  }}
                >
                  Bearbeiten
                </button>
                <button
                  className="btn btn-danger btn-sm mx-4"
                  onClick={() => {
                    handleDelete(customer);
                  }}
                >
                  Löschen
                </button>
              </div>
            );
          })}

        <NewCustomer
          customer={activeCustomer}
          loadCustomers={() => {
            loadCustomers();
          }}
        />
      </header>
    </div>
  );
}

export default App;
