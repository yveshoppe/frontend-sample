import axios from "axios";
import { useEffect, useState } from "react";

const NewCustomer = (props) => {
  const [customer, setCustomer] = useState({ firstName: "", lastName: "" });
  const [error, setError] = useState("");

  const handleCustomerChange = (key, e) => {
    setCustomer({ ...customer, [key]: e.target.value });
  };

  useEffect(() => {
    console.log("NewCustomer.useEffect() called...");

    if (props.customer) {
      setCustomer(JSON.parse(JSON.stringify(props.customer)));
    }
  }, [props.customer]);

  const handleSave = async () => {
    setError("");

    if (!customer.firstName || !customer.lastName) {
      setError("Vor- und Nachname sind Pflichtfelder");
    }

    if (!customer._id) {
      await axios.post("http://localhost:4000/customers", customer);

      setCustomer({ firstName: "", lastName: "" });

      props.loadCustomers();
      return;
    }

    await axios.put("http://localhost:4000/customers", customer);

    setCustomer({ firstName: "", lastName: "" });

    props.loadCustomers();

  };

  return (
    <div className="mt-5">
      <h3>Neuer Kunde</h3>
      <div>
        {error && (
          <div
            style={{
              background: "lightcoral",
              color: "white",
              padding: "10px",
              marginBottom: "10px",
            }}
          >
            {error}
          </div>
        )}

        <input
          type="text"
          placeholder="Vorname"
          className="form-control"
          value={customer.firstName}
          onChange={(e) => handleCustomerChange("firstName", e)}
        />
        <br />
        <input
          type="text"
          className="form-control"
          placeholder="Nachname"
          value={customer.lastName}
          onChange={(e) => handleCustomerChange("lastName", e)}
        />
        <br />
        <button className="btn btn-primary" onClick={handleSave}>
          Speichern
        </button>
      </div>
    </div>
  );
};

export default NewCustomer;
